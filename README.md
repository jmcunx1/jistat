## jistat - clone of istat(1)

jistat(1) is a clone of istat and/or stat(1) for systems
that does not have either.

This is useful if you need a 'standard default' print of
File Information.  istat(1) or stat(1) prints a different
format depending upon the Operating System.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jistat) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jistat.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jistat.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
